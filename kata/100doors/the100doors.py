"""100doors module"""


class The100Doors:
    """the100doors class"""

    def __init__(self):
        self.doorState = []
        for index in range(0, 100):
            self.doorState.append(False)
       
    
    def toggleDoorState(self, currentState):

        if(currentState == False):
            toggled = True
        else:
            toggled = False
            
        return toggled
    
    def checkDoorState(self, index):
        return self.doorState[index-1]
    
    def passThroughDoors(self, times):
        for i in range(1, times+1):
            for j in range (i-1, 100, i):
                self.doorState[j] = self.toggleDoorState(self.checkDoorState(j+1))
        
