# Feature: 100doors

## Narrative:

**As a** customer `<br>`
**I want** to have 100 passes through 100 doors with toggling state of door which index number is divided by number of pass  `<br>`
**So that** I could have opened only doors with index wich are perfect square (1, 4, 9, 16, 25, 36, 49, 64, 81 and 100). `<br>`

## Scenario_1:

**Given**: Toggling door state `<br>`
**When**: try to pass though door `<br>`
**Then**: toggle state of door, open became close and vice versa `<br>`

## Scenario_2:

**Given**: Pass 100 times `<br>`
**When**: pass 100 times  `<br>`
**Then**: only doors with 1, 4, 9, 16, 25, 36, 49, 64, 81 and 100 should be opened `<br>`
