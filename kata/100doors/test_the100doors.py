"""Test module for the100Doors class"""

import unittest

from the100doors import The100Doors


class Test100Doors(unittest.TestCase):
    """Test class for The100Doors class"""
    
    def setUp(self):
        self.doors = The100Doors()
    
    def test_should_return_true_for_false(self):
        self.assertEqual(True, self.doors.toggleDoorState(False))
        
    def test_should_return_false_for_true(self):
        self.assertEqual(False, self.doors.toggleDoorState(True))
        
    def test_should_return_false_for_each_door(self):
        for i in range(1, 100+1):
            self.assertEqual(False, self.doors.checkDoorState(i))
            
    def test_should_return_true_for_1st_door(self):
        self.doors.passThroughDoors(1)
        self.assertEqual(True, self.doors.checkDoorState(1))
        
    def test_should_return_true_for_each_door(self):
        self.doors.passThroughDoors(1)
        for i in range(1, 100+1):
            self.assertEqual(True, self.doors.checkDoorState(i))
            
    def test_should_return_false_for_2nd_door(self):
        self.doors.passThroughDoors(2)
        self.assertEqual(False, self.doors.checkDoorState(2))
        
    def test_should_return_true_for_perfect_square_door_index(self):
        self.doors.passThroughDoors(100)
        for i in [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]:
            self.assertEqual(True, self.doors.checkDoorState(i))
            
    def test_should_return_false_for_not_perfect_square_door_index(self):
        self.doors.passThroughDoors(100)
        
        for i in range(1, 100+1):
            if i not in [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]:
                self.assertEqual(False, self.doors.checkDoorState(i))
                
    def test_should_return_confirm_functionality_is_implemented(self):
        self.doors.passThroughDoors(100)
        
        for i in range(1, 100+1):
            if i not in [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]:
                self.assertEqual(False, self.doors.checkDoorState(i))
                print("The Door number " + str(i) + " is close.")
            else:
                self.assertEqual(True, self.doors.checkDoorState(i))
                print("The Door number " + str(i) + " is open")


if __name__ == '__main__':
    unittest.main()
