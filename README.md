# PuRAS



## Getting started

1. Fork this repository.
2. Finish the course: https://courses.cd.training/courses/tdd-tutorial
3. Select one kata from: https://cyber-dojo.org/creator/home
4. Add your name in [the list](kata/kata_assignees.md) (one person per kata!). You can use https://typora.io/ for editing markdown files. In order to do this, you need to performe following steps:</br>
    a. Create a [ticket](https://gitlab.com/test-driven_development/puras/-/issues/3) on the [Board](https://gitlab.com/test-driven_development/puras/-/boards) </br>
    b. Create a branch and put your name in [the list](kata/kata_assignees.md) (in the commit mention #your_ticket_id).</br>
    c. Create a [Pull Request](https://gitlab.com/test-driven_development/puras/-/merge_requests/2), and if everithing  is ok, I will merge the PR. </br>
    d. After PR is merged, you can continue development on your forked repository.</br>
    ([good example](https://gitlab.com/smarinkov/puras/-/tree/main))</br>
5. Add test_kata.py to .gitlab-ci.yml
6. Follow Red->Green->Refactor cycle during Kata development and commit after each green and refactor stage. Do not commit Red.

Have fun!!!
